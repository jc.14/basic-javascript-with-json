let peopleSalary = {

    "companyStaffs": [
        {
            "id":001,
            "firstName":"Sakapong", 
            "lastName":"Tanmongkol",
            "jobPosition":"CTO",
            "salary":50000
        },
        {
            "id":002,
            "firstName":"Sikkawat", 
            "lastName":"Mayachiew",
            "jobPosition":["CEO", "Product Owner"],
            "salary":70000
        },
        {
            "id":003,
            "firstName":"Worrawat", 
            "lastName":"Fontiem",
            "jobPosition":"Sale Executives",
            "salary":30000
        },
        {
            "id":004,
            "firstName":"Supawit", 
            "lastName":"Charoenwetch",
            "jobPosition":"Managing Director",
            "salary":60000
        }
    ]
}

//New Year's Salary Calculation
for(let x = 0; x < peopleSalary.companyStaffs.length; x++)
{
    let tempSalary = peopleSalary.companyStaffs[x]["salary"];

    peopleSalary.companyStaffs[x]["salary"] = [];
    peopleSalary.companyStaffs[x]["salary"].push(tempSalary);

    let salaryYear = 3; //Define amout of calculatd year
    let salaryIncrease = 10; //Define percentage of salary increasing

    for(let i=0; i<salaryYear; i++) {
        let newSalary = (peopleSalary.companyStaffs[x]["salary"][i]*(100+salaryIncrease)) / 100;
        peopleSalary.companyStaffs[x]["salary"].push(newSalary);
    }

    console.log(peopleSalary.companyStaffs[x]["salary"]);

}

//Create Table
let txt = "";

txt += "<table border= '1' style='width:400px'>";

txt += "<tr>";

//for header
for(title in peopleSalary.companyStaffs[0]) {

    //no jobPosition
    if(title !== "jobPosition") {

        txt += "<th>" + title + "</th>";

    }
    
}

txt += "</tr>";

for(let countP=0; countP<peopleSalary.companyStaffs.length; countP++) {

    txt += "<tr>";

    //access through each staff property value
    for(prop in peopleSalary.companyStaffs[countP]) {

        //check if property is salary --> create an ordered list
        if(prop !== "salary" && prop !== "jobPosition") {

            txt += "<td>" + peopleSalary.companyStaffs[countP][prop] + "</td>";

        } else if (prop === "salary") {

            txt += "<td>";
            txt += "<ol>";

            for(salaryArr in peopleSalary.companyStaffs[countP][prop]) {

                txt += "<li>" + peopleSalary.companyStaffs[countP][prop][salaryArr] + "</li>";

            }

            txt += "</li>";
            txt += "</td>";

        }

    }

    txt += "</tr>";
}

txt += "</table>";

//jquery
$(document).ready(() => {
    $myPage = $("#page");
    $myPage.html(txt);
    $myPage.css('text-align','center');
});

